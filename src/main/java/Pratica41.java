import utfpr.ct.dainf.if62c.pratica.*;

/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
public class Pratica41 {
    public static void main(String[] args) {
        
        Elipse e = new Elipse(4,3);
        System.out.println("Elipse de eixo 4 e 3");
        System.out.println("Area:" + e.getArea());
        System.out.println("Perimetro:" + e.getPerimetro());
        
        System.out.println();
        
        Circulo c = new Circulo(5);
        System.out.println("Circulo de raio 5");
        System.out.println("Area:" + c.getArea());
        System.out.println("Perimetro:" + c.getPerimetro());
        
    }
}

package utfpr.ct.dainf.if62c.pratica;

import java.lang.Math;

/**
 * @author Marcelo G. Costa <marcelo@unicode.com.br>
 */
public class Elipse {
    protected double eixo_x;
    protected double eixo_y;
    
    public Elipse(double semieixo_x, double semieixo_y){
        eixo_x = semieixo_x * 2;
        eixo_y = semieixo_y * 2;
    }
    
    public double getArea(){
        return Math.PI * (eixo_x/2) * (eixo_y/2);
    }
    
    public double getPerimetro(){
        return 
            Math.PI * (
                3 * (
                    (eixo_x/2) + (eixo_y/2)
                ) 
                - Math.sqrt(
                    (
                        (3 * (eixo_x/2)) + (eixo_y/2)
                    )*(
                        ((eixo_x/2)) + 3 * (eixo_y/2)
                    )
                )
            );
    }
}

